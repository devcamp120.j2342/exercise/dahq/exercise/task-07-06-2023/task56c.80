package com.devcamp.musicapi.model;

public class Composer extends Person {
    private String stageName;

    public Composer(String firstName, String lastName) {
        super(firstName, lastName);

    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public Composer(String firstName, String lastName, String stageName) {
        super(firstName, lastName);
        this.stageName = stageName;
    }

    @Override
    public String toString() {
        return "Composer [stageName=" + stageName + super.toString() + "]";
    }

}
