package com.devcamp.musicapi.model;

import java.util.List;

public class Artist extends Composer {
    private List<Album> albums;

    public Artist(String firstName, String lastName, String stageName) {
        super(firstName, lastName, stageName);

    }

    public Artist(String firstName, String lastName) {
        super(firstName, lastName);

    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    public Artist(String firstName, String lastName, List<Album> albums) {
        super(firstName, lastName);
        this.albums = albums;
    }

    public Artist(String firstName, String lastName, String stageName, List<Album> albums) {
        super(firstName, lastName, stageName);
        this.albums = albums;
    }

    @Override
    public String toString() {
        return "Artist [albums=" + albums + super.toString() + "]";
    }

}
