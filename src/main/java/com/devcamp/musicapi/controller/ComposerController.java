package com.devcamp.musicapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.musicapi.model.Artist;
import com.devcamp.musicapi.model.Band;
import com.devcamp.musicapi.service.ArtistService;
import com.devcamp.musicapi.service.ComposerService;

@RestController
@RequestMapping("/")
@CrossOrigin

public class ComposerController {
    @Autowired
    private ComposerService composerService;

    @GetMapping("/bands")
    public ArrayList<Band> bandsList() {
        ArrayList<Band> bands = composerService.allBands();
        return bands;

    }

    @Autowired
    private ArtistService artistService;

    @GetMapping("/artists")
    public ArrayList<Artist> artistsList() {
        ArrayList<Artist> artists = artistService.allArtists();
        return artists;
    }

}
