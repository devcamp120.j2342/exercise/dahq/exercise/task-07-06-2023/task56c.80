package com.devcamp.musicapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.musicapi.model.Album;
import com.devcamp.musicapi.service.AlbumService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AlbumController {
    @Autowired
    private AlbumService albumService;

    @GetMapping("/albums")
    public ArrayList<Album> albumsList() {
        ArrayList<Album> albums = albumService.allAlbums();
        return albums;
    }

}
