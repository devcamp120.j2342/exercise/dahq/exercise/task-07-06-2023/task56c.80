package com.devcamp.musicapi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.musicapi.model.Album;

@Service
public class AlbumService {
    Album album1 = new Album("Album1");
    Album album2 = new Album("Album2");
    Album album3 = new Album("Album3");
    Album album4 = new Album("Album4");
    Album album5 = new Album("Album5");
    Album album6 = new Album("Album6");
    Album album7 = new Album("Album7");
    Album album8 = new Album("Album8");
    Album album9 = new Album("Album9");
    Album album10 = new Album("Album10");
    List<String> song1s = new ArrayList<>();
    List<String> song2s = new ArrayList<>();
    List<String> song3s = new ArrayList<>();
    List<String> song4s = new ArrayList<>();
    List<String> song5s = new ArrayList<>();
    List<String> song6s = new ArrayList<>();
    List<String> song7s = new ArrayList<>();
    List<String> song8s = new ArrayList<>();
    List<String> song9s = new ArrayList<>();
    List<String> song10s = new ArrayList<>();

    public ArrayList<Album> album1s() {
        song1s.add("On my Way");
        song1s.add("Attention");
        song2s.add("One Call Away");
        song2s.add("We Don't Talk Anymore");
        album1.setSongs(song1s);
        album2.setSongs(song2s);
        ArrayList<Album> albums = new ArrayList<>();
        albums.add(album1);
        albums.add(album2);
        return albums;
    }

    public ArrayList<Album> album2s() {
        song3s.add("Marvin Gaye");
        song3s.add("How Long");
        song4s.add("Done for Me");
        song4s.add("See You Again");
        song5s.add("Mother");
        song5s.add("Girlfriend");
        song6s.add("Slow It Down");
        song6s.add("LA Girls");
        album3.setSongs(song3s);
        album4.setSongs(song4s);
        album5.setSongs(song5s);
        album6.setSongs(song6s);
        ArrayList<Album> albums = new ArrayList<>();
        albums.add(album3);
        albums.add(album4);
        albums.add(album5);
        albums.add(album6);

        return albums;
    }

    public ArrayList<Album> album3s() {
        song7s.add("Empty Cups");
        song8s.add("Suffer");
        song8s.add("Dangerously");
        song9s.add("River");
        song9s.add("Losing My Mind");
        song10s.add("Some Type of Love");
        song10s.add("As You Are");
        album7.setSongs(song7s);
        album8.setSongs(song8s);
        album9.setSongs(song9s);
        album10.setSongs(song10s);
        ArrayList<Album> albums = new ArrayList<>();

        albums.add(album7);
        albums.add(album8);
        albums.add(album9);
        albums.add(album10);
        return albums;
    }

    public ArrayList<Album> allAlbums() {
        ArrayList<Album> albums = new ArrayList<>();
        albums.addAll(album1s());
        albums.addAll(album2s());
        albums.addAll(album3s());
        return albums;

    }

}
