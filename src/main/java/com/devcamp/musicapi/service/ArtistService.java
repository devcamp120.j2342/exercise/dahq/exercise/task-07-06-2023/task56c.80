package com.devcamp.musicapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.musicapi.model.Artist;
import com.devcamp.musicapi.model.Composer;

@Service
public class ArtistService {
    @Autowired
    private ComposerService composerService;

    public ArrayList<Artist> allArtists() {
        ArrayList<Composer> composers = composerService.allComposers();
        ArrayList<Artist> artists = new ArrayList<>();
        for (Composer composer : composers) {
            if (composer instanceof Artist) {
                artists.add((Artist) composer);
            }
        }
        return artists;
    }

}
