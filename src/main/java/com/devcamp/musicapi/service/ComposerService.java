package com.devcamp.musicapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.musicapi.model.Artist;
import com.devcamp.musicapi.model.Band;
import com.devcamp.musicapi.model.BrandMember;
import com.devcamp.musicapi.model.Composer;

@Service
public class ComposerService {
    @Autowired
    private AlbumService albumService;
    Artist artist1 = new Artist("Nguyen Van", "A", "Aaa");
    Artist artist2 = new Artist("Nguyen Van", "B", "Bbb");
    Artist artist3 = new Artist("Nguyen Van", "C", "Ccc");
    BrandMember brandMember1 = new BrandMember("Band", "RockA", "RcA", "drum");
    BrandMember brandMember2 = new BrandMember("Band", "RockB", "RcA", "drum");
    BrandMember brandMember3 = new BrandMember("Band", "RockC", "RcA", "drum");
    BrandMember brandMember4 = new BrandMember("Band", "RockD", "RcA", "drum");
    BrandMember brandMember5 = new BrandMember("Band", "RockE", "RcA", "drum");
    BrandMember brandMember6 = new BrandMember("Band", "RockF", "RcA", "drum");

    public ArrayList<Composer> allComposers() {
        artist1.setAlbums(albumService.album1s());
        artist2.setAlbums(albumService.album2s());
        artist3.setAlbums(albumService.album3s());
        ArrayList<Composer> composers = new ArrayList<>();
        composers.add(brandMember1);
        composers.add(brandMember2);
        composers.add(brandMember3);
        composers.add(brandMember4);
        composers.add(brandMember5);
        composers.add(brandMember6);
        composers.add(artist1);
        composers.add(artist2);
        composers.add(artist3);
        return composers;

    }

    public ArrayList<Band> allBands() {
        Band band1 = new Band("Drum1");
        Band band2 = new Band("Drum2");
        ArrayList<BrandMember> brandMembers = new ArrayList<>();
        brandMembers.add(brandMember1);
        brandMembers.add(brandMember2);
        brandMembers.add(brandMember3);
        ArrayList<BrandMember> brandMembersGr1 = new ArrayList<>();
        brandMembersGr1.add(brandMember4);
        brandMembersGr1.add(brandMember5);
        brandMembersGr1.add(brandMember6);

        band1.setBrandMembers(brandMembersGr1);
        band2.setBrandMembers(brandMembers);
        band1.setAlbums(albumService.album1s());
        band2.setAlbums(albumService.album2s());
        ArrayList<Band> bands = new ArrayList<>();
        bands.add(band2);
        bands.add(band1);
        return bands;

    }
}
